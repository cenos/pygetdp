#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# License: MIT


from pygetdp import PostOperation, render


def test_postoperation():
    print("\n----------------\n")
    pp = PostOperation()
    p0 = pp.add("Map_v_e", "EleSta_v")
    qt = p0.add()
    qt.add("v", OnElementsOf="Domain", File="map_v.pos")
    qt.add(quantity="e", OnElementsOf="Domain", File="map_e.pos", append=True)

    render(pp.code)
    ref = """PostOperation{\n    { Name Map_v_e; NameOfPostProcessing EleSta_v; \n         Operation {  \n            Print [ v, OnElementsOf Domain, File "map_v.pos" ]; \n            Print [ e, OnElementsOf Domain, File  > "map_e.pos" ]; \n            }\n    }\n    }\n"""
    assert pp.code == ref
