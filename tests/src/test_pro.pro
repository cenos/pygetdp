// This code was created by pygetdp v1.0.0.

/*  THIS IS AN EXAMPLE OF A PRO FILE THIS IS AN EXAMPLE OF A PRO FILE THIS
IS AN EXAMPLE OF A PRO FILE THIS IS AN EXAMPLE OF A PRO FILE THIS IS
AN EXAMPLE OF A PRO FILE THIS IS AN EXAMPLE OF A PRO FILE THIS IS AN
EXAMPLE OF A PRO FILE THIS IS AN EXAMPLE OF A PRO FILE THIS IS AN
EXAMPLE OF A PRO FILE THIS IS AN EXAMPLE OF A PRO FILE */

Include "some_file.txt";

Include "params.dat"; 

Group{
    DefineGroup[Air, Core];
    Air = Region[ 1000 ];
    Air2 = Region[ 1001 ];// Air region
    Air3 = Region[ 1002 ];
    Core = Region[ {2000, 3000} ];
    Inductor = Region[ 1 ];
    NonConductingDomain = Region[ {Air, Core} ];
    }

Function{
    DefineFunction[epsilon, mu];
    kx = 3 ;// constant
    e[air] = 3.22 ;
    f[] = kx + 1 ;
    rho[] = $1^2+$2^2+kx ;
    g[#{Air, Core}] = 3*$1^3-1/$2^2+Sin[$X] ;// some function defined in a sub domain
    epsilon[air] = 1 ;
    epsilon[core] = 13 ;
    z0 = Complex[2.1, -4.7] ;
    z1 = Complex[12.0, -3.0] ;
    epsilon_tens[air] = 1 * TensorDiag[1,1,1] ;
    epsilon_tens[core] = 13 * TensorDiag[1,1,1] ;
    a = Complex[1.0, 1.0] ;
    b = Cos[1.2] ;// a getdp expression
    c = 3.32 ;
    d = 12 ;// description
    }

Constraint{
    }

FunctionSpace{
    }

Jacobian{
    }

Integration{
    }

Formulation{
    }

Resolution{
    }

PostProcessing{
    }

PostOperation{
    }
