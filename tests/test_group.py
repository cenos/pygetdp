#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# License: MIT


from pygetdp import Group


def test_group():
    print("\n----------------\n")
    group = Group()
    group.comment = "Definition of topological entities"
    group.define(id=["Air", "Core"])
    air = group.add(id="Air", glist=[1000])
    group.Region(id="Air2", glist=[1001], comment="Air region")
    group.Region("Air3", 1002)
    core = group.Region("Core", [2000, 3000])
    inductor = group.Region("Inductor")
    group.Region("NonConductingDomain", [air, core])
    group.Region("ConductingDomain", inductor)
    group.NodesOf("Dom2", ["NonConductingDomain"], Not="Skin")
    group.ElementsOf("q", "a", OnOneSideOf="x", OnPositiveSideOf="e", Not="b")
    group.FacetsOfTreeIn("Q", "z", StartingOn="s")
    print(group.idlist)

    ref = "// Definition of topological entities\nGroup{\n    DefineGroup[Air, Core];\n    Air = Region[ 1000 ];\n    Air2 = Region[ 1001 ];// Air region\n    Air3 = Region[ 1002 ];\n    Core = Region[ {2000, 3000} ];\n    Inductor = Region[ 1 ];\n    NonConductingDomain = Region[ {Air, Core} ];\n    ConductingDomain = Region[ Inductor ];\n    Dom2 = NodesOf[ NonConductingDomain , Not Skin];\n    q = ElementsOf[ a , OnOneSideOf x, OnPositiveSideOf e, Not b];\n    Q = FacetsOfTreeIn[ z , StartingOn s];\n    }\n"

    print(group.code)
    assert group.code == ref
