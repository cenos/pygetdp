#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# License: MIT


from pygetdp import PostProcessing, render


def test_postprocessing():
    print("\n----------------\n")
    pp = PostProcessing()
    p0 = pp.add("test", "formulation")
    qt = p0.add()
    
    qt.add(
        Name = "a",
        Type = "Local",
        Values = [
            {"val":"{a1}", "in":"Somewhere", "jac":"Jac1", 'int':None},
            {"val":"0", "in":"Anywhere", "jac":"Jac2", 'int':'Coolregion'}
            ]
        )
    
    qt.add(
        Name = "b",
        Type = "Integral",
        Values = [
            {"val":"{b}", "in":"Somewhere", "jac":"Jac", 'int':None}
            ]
        )

    render(pp.code)
    
    ref = """
    PostProcessing{
    { Name test; NameOfFormulation formulation; 
         Quantity {  
            {  Name a; Value {  
               Local {  [ {a1} ]; In Somewhere; Jacobian Jac1;  }
               Local {  [ 0 ]; In Anywhere; Jacobian Jac2;  }
               Integration Coolregion;
               } }
            {  Name b; Value {  
               Integral {  [ {b} ]; In Somewhere; Jacobian Jac;  }
               } }
            }
    }
    }
    """
    
    print(ref)
    assert pp.code == ref
