from pygetdp import Function, render


def test_function():
    print("\n----------------\n")
    function = Function()
    function.define(name=["epsilon", "mu"])
    function.constant(name="kx", expression="3", comment="constant")
    function.add(name="e", expression=3.22, region="air")
    function.add(name="f", expression="kx + 1")
    function.add(name="rho", expression="x^2+y^2 + kx", arguments=["x", "y"])
    function.add(
        "g",
        expression="3 * length^3 - 1 / width^2 + Sin[$X]",
        arguments=["length", "width"],
        region=["air", "core"],
        comment="some function defined in a sub domain",
    )
    regions = ["air", "core"]
    epsi = [1, 13]

    for r, value in zip(regions, epsi):
        function.add("epsilon", r, value)

    z0 = function.Complex(1, 2)

    function.constant("z0", z0)
    function.constant("z1", 12 - 3 * 1j)
    Id = function.TensorDiag(1, 1, 1)
    for r, value in zip(regions, epsi):
        function.add("epsilon_tens", r, str(value) + " * " + Id)

    params = {}
    params["a"] = 1 + 1j
    params["b"] = "Cos[1.2]"
    params["c"] = 3.32
    params["d"] = 12, "description"

    function.add_params(params)
    print(function.code)

    ref = (
        "Function{\n    DefineFunction[epsilon, mu];\n    kx = 3 ;// constant\n    "
        "e[air] = 3.22 ;\n    f[] = kx + 1 ;\n    rho[] = $1^2+$2^2+kx ;\n    "
        "g[#{air, core}] = 3*$1^3-1/$2^2+Sin[$X] ;// some function defined in a sub domain\n    "
        "epsilon[1] = air ;\n    epsilon[13] = core ;\n    z0 = Complex[1, 2] ;\n    "
        "z1 = Complex[12.0, -3.0] ;\n    epsilon_tens[1 * TensorDiag[1,1,1]] = air ;\n    "
        "epsilon_tens[13 * TensorDiag[1,1,1]] = core ;\n    a = Complex[1.0, 1.0] ;\n    "
        "b = Cos[1.2] ;\n    c = 3.32 ;\n    d = 12 ;// description\n    }\n"
    )
    assert function.code == ref

    function.add("list", [1, 2, 3, 4, 5, 6])
    render(function.code)
