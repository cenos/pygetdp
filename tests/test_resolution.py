#!/usr/bin/python
# -*- coding: utf-8 -*-
#


from pygetdp import Resolution, render


def test_resolution():
    #
    # tl = TimeLoopTheta()
    # tl.Generate("Sys_Mag")
    # tl.Solve("Sys_Mag")
    # tl.SaveSolution("Sys_Mag")
    #
    # render(tl.code)
    #
    # op = Operation()
    # tl = op.add_time_loop_theta()
    # tl.Generate("Sys_Mag")
    # tl.Solve("Sys_Mag")
    # tl.SaveSolution("Sys_Mag")
    #
    # render(tl.code)
    # render(op.code)

    # Static resolution (electrostatic problem)
    resolution = Resolution()
    electrostatics_v = resolution.add(Name="Electrostatics_v")
    s = electrostatics_v.add_system()
    s.add(Name="Sys_Ele", NameOfFormulation="Electrostatics_v")
    op = electrostatics_v.add_operation()
    op.Generate("Sys_Ele")
    op.Solve("Sys_Ele")
    op.SaveSolution("Sys_Ele")
    render(resolution.code)

    # Frequency domain resolution (magnetodynamic problem)
    resolution = Resolution()
    Magnetodynamics_hphi = resolution.add(Name="Magnetodynamics_hphi")
    s = Magnetodynamics_hphi.add_system()
    s.add(Name="Sys_Mag", NameOfFormulation="Magnetodynamics_hphi", Frequency="Freq")
    op = Magnetodynamics_hphi.add_operation()
    op.Generate("Sys_Mag")
    op.Solve("Sys_Mag")
    op.SaveSolution("Sys_Mag")
    render(resolution.code)

    # Time domain resolution (magnetodynamic problem)
    resolution = Resolution()
    Magnetodynamics_hphi_Time = resolution.add(Name="Magnetodynamics_hphi_Time")
    s = Magnetodynamics_hphi_Time.add_system()
    s.add(Name="Sys_Mag", NameOfFormulation="Magnetodynamics_hphi")
    op = Magnetodynamics_hphi_Time.add_operation()
    op.InitSolution("Sys_Mag")
    op.SaveSolution("Sys_Mag")
    tl = op.add_time_loop_theta(
        "Mag_Time0", "Mag_TimeMax", "Mag_DTime[]", "Mag_Theta[]"
    )
    tl.Generate("Sys_Mag")
    tl.Solve("Sys_Mag")
    tl.SaveSolution("Sys_Mag")
    render(resolution.code)
    # ref = """Jacobian{\n    { Name Vol; \n         Case  {  \n           { Region All; Jacobian Vol; } \n         }\n    }\n    { Name Sur; \n         Case  {  \n           { Region All; Jacobian Sur; } \n         }\n    }\n    { Name Lin; \n         Case  {  \n           { Region All; Jacobian Lin; } \n         }\n    }\n    }\n"""

    # assert jac.code == ref
