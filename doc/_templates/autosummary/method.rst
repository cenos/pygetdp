:orphan:

{{ fullname }}
{{ underline }}

.. currentmodule:: {{ module }}

.. automethod:: {{ objname }}




.. include:: backreferences/{{module}}.{{objname}}.examples

.. raw:: html

    <div class="clearer"></div>
