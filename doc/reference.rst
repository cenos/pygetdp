
PyGetDP
=======

:Release: |release|
:Date: |today|

PyGetDP is open-source software for creating GetDP problems descriptions and
solving the relevant partial differential equations with the Finite Element method.

.. .. toctree::
..    :maxdepth: 1
..
..    install_upgrade
..    api
..    release

Examples
--------

Tutorials with worked examples and background information for most PyGetDP
submodules.

.. toctree::
   :maxdepth: 2

   auto_examples/index.rst




API Reference
-------------

The exact API of all functions and classes, as given by the docstrings. The API
documents expected types and allowed features for all functions, and all
parameters available for the algorithms.

..
..
.. .. automodule:: pygetdp
..    :no-members:
..    :no-inherited-members:
..    :no-special-members:


.. module:: pygetdp

.. autosummary::
  :toctree: generated/

  Group
  Function
  FunctionSpace
  Jacobian
  Integration
  Formulation
  Resolution
  PostProcessing
  PostOperation





-------------------------------------


Indices and search
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
