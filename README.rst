
.. image:: https://gitlab.com/benvial/pygetdp/badges/master/pipeline.svg
   :target: https://gitlab.com/benvial/pygetdp/commits/master
   :alt: pipeline status

.. image:: https://gitlab.com/benvial/pygetdp/badges/master/coverage.svg
  :target: https://gitlab.com/benvial/pygetdp/commits/master
  :alt: coverage report

.. image:: https://img.shields.io/github/license/mashape/apistatus.svg
   :alt: Licence: MIT

.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :alt: Code style: black

pygetdp
=======


`Python <http://www.python.org/>`_ frontend for `GetDP <http://www.getdp.info/>`_.

`benvial.gitlab.io/pygetdp <https://benvial.gitlab.io/pygetdp>`_

.. inclusion-marker-do-not-remove


.. image:: https://gitlab.com/benvial/pygetdp/-/jobs/artifacts/master/raw/public/_images/getdp.png?job=pages
   :width: 90%
   :align: center
