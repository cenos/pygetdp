# -*- coding: utf-8 -*-
"""
Function
==========================================

An example to define a Function object
"""


from numpy import pi

from pygetdp import Function
from pygetdp.helpers import build_example_png, print_html

##############################################################################
# A physical characteristic is a piecewise defined function.
# The magnetic permeability ``mu`` can for example be defined in the considered
# regions by

function = Function()
function.constant(name="mu0", expression=4 * pi * 1e-7, comment="vacuum permeability")
function.add(name="mu", expression="mu0", region="Air")
function.add(name="mu", expression="1000*mu0", region="Core")

print_html(function.code, tmp_file="out0.html")

####################################
# .. raw:: html
#     :file: out0.html


##############################################################################
# A nonlinear characteristic can be defined through an expression with arguments, e.g.,

function = Function()
function.constant("a1", 1000, comment="constant")
function.constant("b1", 100, comment="constant")
function.add(name="mu", expression="mu0", region="Air")
function.add(
    name="mu", expression="mu0 + 1./(a1+b1*Norm[$1]^6)", region="NonlinearCore"
)

print_html(function.code, tmp_file="out1.html")

####################################
# .. raw:: html
#     :file: out1.html


##############################################################################
# where function ``mu[]`` in region ``NonLinearCore`` has one argument ``$1`` which has to
# be the magnetic flux density. This function is actually called when writing
# the equations of a formulation, which permits to directly extend it to
# a nonlinear form by adding only the necessary arguments. For example,
# in a magnetic vector potential formulation, one may write ``mu[{Curl a}]``
# instead of ``mu[]`` in Equation terms (see Formulation examples). Multiple
# arguments can be specified in a similar way: writing ``mu[{Curl a},{T}]`` in
# an Equation term will provide the function ``mu[]`` with two usable arguments,
# ``$1`` (the magnetic flux density) and ``$2`` (the temperature). In pygetdp one would write:

function = Function()
function.add(
    "mu",
    expression="mu0 + {T}/(a1+b1*Norm[{Curl a}]^6)",
    arguments=["{Curl a}", "{T}"],
    region=["NonlinearCore"],
)

print_html(function.code, tmp_file="out2.html")

####################################
# .. raw:: html
#     :file: out2.html


##############################################################################
# A function can also be time dependent, e.g.,


f = Function()
f.constant("Freq", 50)
f.constant("Phase", "30./180.*Pi")
f.add("TimeFct_Sin", expression=f.Sin(" 2.*Pi*Freq * $Time + Phase "))
f.add("TimeFct_Exp", expression=f.Exp("- $Time / 0.0119"))
f.add("TimeFct_Sin", expression=f.Sin_wt_p("2.*Pi*Freq", "Phase"))

print_html(f.code, tmp_file="out3.html")

####################################
# .. raw:: html
#     :file: out3.html


##############################################################################
# Finally we write and render the code as png

build_example_png(f.code)
